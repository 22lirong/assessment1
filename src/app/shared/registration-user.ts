export class RegistrationUser {
    constructor(
        public email: string,
        public password: string,
        public fullName: string,
        public gender: string,
        public dateOfBirth: Date,
        public address: string,
        public country: string,
        public contactNumber: string,
    ){

    }
}