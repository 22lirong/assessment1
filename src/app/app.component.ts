import { Component, OnInit } from '@angular/core';
import { RegistrationUser } from './shared/registration-user';
import { UserRegistrationService } from './services/user-registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'app';
  model = null;
  gender: string[] = ['M', 'F'];
  isSubmitted: boolean = false;

  currentDate = new Date();
    
  isUnderAge: boolean = true;

  constructor(private userService: UserRegistrationService){

  }

  ngOnInit() {
    this.model = new RegistrationUser('','','','',null,'','','');
  }

  onSubmit() {
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.fullName);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.country);
    console.log(this.model.contactNumber);

    this.userService.saveUserRegistration(this.model).subscribe(users => {
      console.log('send to backend!');
    })

    this.isSubmitted = true;
  }

  onChange(event) {
    console.log('dateOfBirth: ', this.model.dateOfBirth);
    console.log('isUnderAge: ', this.isUnderAge);
    
    // convert dateOfBirth to mSec
    var birthday = Date.parse(this.model.dateOfBirth);
    
    // today mSec - birthday mSec
    var timeDiff = (Date.now() - birthday);
    
    // returns the largest integer less than or equal to
    var age = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
    
    console.log("age: ", age);

    if (age < 18) {
      this.isUnderAge = true;
    } else {
      this.isUnderAge = false;
    }

    console.log('isUnderAge: ', this.isUnderAge);

  }

}