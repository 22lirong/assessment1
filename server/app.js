/*
Server side app
*/

console.log("Starting server side app ...");

/* start: import libs */
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
/* end: import libs */

/* instantiate and configure ExpressJS for the backend server side app.*/
var app = express();
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(cors());

console.log(__dirname);

/* the NODE_PORT variable is derived from the operating system enviroment variable */
const NODE_PORT = process.env.PORT;

app.use(express.static(__dirname + "/../dist/"));

// get, post, put, delete - HTTP method
app.post("/api/user/register", (req, res)=>{
// console.log(req);
var user = req.body;
console.log(user);
res.status(200).json(user);
});

/* incorporate Express JS listening codes into the server/app.js */
app.listen(NODE_PORT, function(){
console.log(`Backend Server started at ${NODE_PORT}`);
})
